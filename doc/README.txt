Aluno: João Lucas Sousa Reis	
Matrícula: 160009758


	No seu Trabalho Prático de Orientação à Objetos da Universidade de Brasília Campus Gama foi a elaboração de uma Calculado cuja faça gráficos de cálculos do curso de Engenharia de Energia também do Campus do Gama.
	O trabalho consistia como uma encomenda, com eu e outros alunos da disciplina pretedemos cursar o curso de Engenharia de Software devemos está preparados para qualquer tipo de trabalho e essoa foi a intenção do trabalho.
	O trabalho consiste em duas partes,uma para calcular o Simulador de Potência Fundamental e a outra Distorção Harmônica, a pedido do Professor, foi elaborado uma calculadora que apresenta tais resultados da operação além de representar graficamente os resultados.

	Para utilizar a calculadora Simulador de Potência Fundamental:

	1 - Preencha os ângulos e a amplitude da tensão e da corrente de acordo com os números máximos demostrados como legenda no programa.
	2 - Clique no Botão Simular/Calcular para obter os resultados em Gráfico e em valores.

	Pra utilizar a calculado Distorção Harmônica:

	1 - Preencha a Amplitude e o Ângulo de acordo com a legenda.
	2 - Preencha o Números de interações harmônicas que deseja (Limite até 5) e Marque a opção desejada se par ou ímpar
	3 - Clique no Botão Simular/Calcular para obter os resultados em Gráfico e em valores.


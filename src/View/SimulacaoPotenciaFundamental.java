package View;

import java.awt.EventQueue;

import java.util.ArrayList;
import java.util.List;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSpinner;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.DebugGraphics;
import javax.swing.JButton;
import java.awt.SystemColor;
import Model.GraphPanel;
import Controler.ControlerSimulacaoPotenciaFundamental;

public class SimulacaoPotenciaFundamental extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private static final String TITULO = "SIMULAÇÃO POTÊNCIA FUNDAMENTAL";
	private JPanel contentPane;
	public static JTextField textPotenciaReativa;
	public static JTextField textPotenciaAparente;
	public static JTextField textPotenciaAtiva;
	public static JTextField textFatorPotencia;
	 
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SimulacaoPotenciaFundamental frame = new SimulacaoPotenciaFundamental();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SimulacaoPotenciaFundamental() {
		setBackground(Color.GRAY);
		setTitle(TITULO);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1000, 650);
		contentPane = new JPanel();
		contentPane.setBackground(Color.GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblSimulacao = new JLabel("Adicione os Valores: ");
		lblSimulacao.setFont(new Font("Dialog", Font.BOLD, 25));
		lblSimulacao.setForeground(SystemColor.desktop);
		lblSimulacao.setBounds(6, 12, 367, 18);
		contentPane.add(lblSimulacao);
		
		JLabel lblEntradas = new JLabel("Entradas: ");
		lblEntradas.setFont(new Font("Dialog", Font.BOLD, 40));
		lblEntradas.setForeground(SystemColor.activeCaptionText);
		lblEntradas.setBounds(416, 12, 257, 81);
		contentPane.add(lblEntradas);
		
		JLabel lblTensao = new JLabel("Tensão");
		lblTensao.setForeground(Color.WHITE);
		lblTensao.setFont(new Font("Dialog", Font.BOLD, 15));
		lblTensao.setBounds(163, 75, 91, 15);
		contentPane.add(lblTensao);
		
		JLabel lblAnguloTensao = new JLabel("Ângulo de Fase\n");
		lblAnguloTensao.setForeground(Color.WHITE);
		lblAnguloTensao.setBounds(40, 99, 163, 18);
		contentPane.add(lblAnguloTensao);
		
		JSpinner spinnerAnguloTensao = new JSpinner();
		spinnerAnguloTensao.setModel(new SpinnerNumberModel(0, -180, 180, 1));		
		spinnerAnguloTensao.setBounds(25, 129, 151, 20);
		contentPane.add(spinnerAnguloTensao);
		
		JLabel lblVeff = new JLabel("VEFF");
		lblVeff.setForeground(Color.WHITE);
		lblVeff.setBounds(172, 161, 114, 15);
		contentPane.add(lblVeff);
		
		JProgressBar Veff = new JProgressBar();
		Veff.setOrientation(SwingConstants.VERTICAL);
		Veff.setDebugGraphicsOptions(DebugGraphics.NONE_OPTION);
		Veff.setAutoscrolls(true);
		Veff.setBounds(182, 188, 14, 70);
		contentPane.add(Veff);
		
		JLabel lblAmplitudeTensao = new JLabel("Amplitude");
		lblAmplitudeTensao.setForeground(Color.WHITE);
		lblAmplitudeTensao.setBounds(247, 101, 198, 15);
		contentPane.add(lblAmplitudeTensao);
		
		JSpinner spinnerAmplitudeTensao = new JSpinner();
		spinnerAmplitudeTensao.setModel(new SpinnerNumberModel(0, 0, 220, 1));
		spinnerAmplitudeTensao.setBounds(207, 129, 151, 20);
		contentPane.add(spinnerAmplitudeTensao);
		
		JLabel lblCorrente = new JLabel("Corrente");
		lblCorrente.setFont(new Font("Dialog", Font.BOLD, 15));
		lblCorrente.setForeground(Color.WHITE);
		lblCorrente.setBounds(802, 54, 198, 15);
		contentPane.add(lblCorrente);
		
		JLabel lblAnguloCorrente = new JLabel("Ângulo de Fase");
		lblAnguloCorrente.setForeground(Color.WHITE);
		lblAnguloCorrente.setBounds(850, 101, 198, 15);
		contentPane.add(lblAnguloCorrente);
		
		JSpinner spinnerAnguloCorrente = new JSpinner();
		spinnerAnguloCorrente.setModel(new SpinnerNumberModel(0, -180, 180, 1));
		spinnerAnguloCorrente.setBounds(836, 129, 151, 20);
		contentPane.add(spinnerAnguloCorrente);
		
		JLabel lblAeff = new JLabel("AEFF");
		lblAeff.setForeground(Color.WHITE);
		lblAeff.setBounds(823, 161, 114, 15);
		contentPane.add(lblAeff);
		
		JProgressBar Aeff = new JProgressBar();
		Aeff.setOrientation(SwingConstants.VERTICAL);
		Aeff.setDebugGraphicsOptions(DebugGraphics.NONE_OPTION);
		Aeff.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		Aeff.setAutoscrolls(true);
		Aeff.setBounds(833, 188, 14, 70);
		contentPane.add(Aeff);
		
		JLabel lblAmplitudeCorrente = new JLabel("Amplitude");
		lblAmplitudeCorrente.setForeground(Color.WHITE);
		lblAmplitudeCorrente.setBounds(720, 99, 198, 15);
		contentPane.add(lblAmplitudeCorrente);
		
		JSpinner spinnerAmplitudeCorrente = new JSpinner();
		spinnerAmplitudeCorrente.setModel(new SpinnerNumberModel(0, 0, 100, 1));
		spinnerAmplitudeCorrente.setBounds(673, 129, 151, 20);
		contentPane.add(spinnerAmplitudeCorrente);
		
		JLabel lblResultados = new JLabel("Resultados: ");
		lblResultados.setFont(new Font("Dialog", Font.BOLD, 30));
		lblResultados.setForeground(SystemColor.activeCaptionText);
		lblResultados.setBounds(416, 380, 310, 50);
		contentPane.add(lblResultados);
		
		JLabel lblValorDaPotencia = new JLabel("Valor da Potência Ativa: ");
		lblValorDaPotencia.setForeground(Color.WHITE);
		lblValorDaPotencia.setBounds(40, 492, 177, 15);
		contentPane.add(lblValorDaPotencia);
		
		textPotenciaReativa = new JTextField();
		textPotenciaReativa.setEditable(false);
		textPotenciaReativa.setBounds(247, 519, 101, 19);
		contentPane.add(textPotenciaReativa);
		textPotenciaReativa.setColumns(10);
		
		JLabel lblValorDaPotncia_1 = new JLabel("Valor da Potência Reativa: ");
		lblValorDaPotncia_1.setForeground(Color.WHITE);
		lblValorDaPotncia_1.setBounds(40, 519, 203, 15);
		contentPane.add(lblValorDaPotncia_1);
		
		textPotenciaAparente = new JTextField();
		textPotenciaAparente.setEditable(false);
		textPotenciaAparente.setBounds(247, 546, 101, 19);
		contentPane.add(textPotenciaAparente);
		textPotenciaAparente.setColumns(10);
		
		JLabel lblValorDaPotncia_2 = new JLabel("Valor da Potência Aparente: ");
		lblValorDaPotncia_2.setForeground(Color.WHITE);
		lblValorDaPotncia_2.setBounds(38, 546, 216, 18);
		contentPane.add(lblValorDaPotncia_2);
		
		textPotenciaAtiva = new JTextField();
		textPotenciaAtiva.setEditable(false);
		textPotenciaAtiva.setBounds(247, 488, 101, 19);
		contentPane.add(textPotenciaAtiva);
		textPotenciaAtiva.setColumns(10);
		
		JLabel lblValorDoFator = new JLabel("Valor do Fator de Potência: ");
		lblValorDoFator.setForeground(Color.WHITE);
		lblValorDoFator.setBounds(40, 576, 203, 15);
		contentPane.add(lblValorDoFator);
		
		textFatorPotencia = new JTextField();
		textFatorPotencia.setEditable(false);
		textFatorPotencia.setBounds(247, 574, 101, 19);
		contentPane.add(textFatorPotencia);
		textFatorPotencia.setColumns(10);
		
		List<Double> scores3 = new ArrayList<>();
		GraphPanel panel = new Model.GraphPanel(scores3);
		panel.setOpaque(false);
		panel.setBorder(null);
		panel.setBackground(new Color(238, 238, 238));
		panel.setBounds(426,442,300,175);
		panel.setPreferredSize(new Dimension(800, 600));
		contentPane.add(panel);
		
		List<Double> scores2 = new ArrayList<>();
		GraphPanel panelTesao = new Model.GraphPanel(scores2);
		panelTesao.setOpaque(false);
		panelTesao.setBorder(null);
		panelTesao.setBackground(new Color(238, 238, 238));
		panelTesao.setBounds(25,273,300,175);
		panelTesao.setPreferredSize(new Dimension(800, 600));
		contentPane.add(panelTesao);
		
		List<Double> scores = new ArrayList<>();
		int MaxDatePoints = 50;
		GraphPanel panelPotencia = new Model.GraphPanel(scores);
		panelPotencia.setOpaque(false);
		panelPotencia.setBorder(null);
		panelPotencia.setBackground(new Color(238, 238, 238));
		panelPotencia.setBounds(719,270,300,175);
		panelPotencia.setPreferredSize(new Dimension(800, 600));
		contentPane.add(panelPotencia);
		
		JButton button1 = new JButton("SIMULAR/CALCULAR");
		button1.setFont(new Font("DejaVu Sans Condensed", Font.BOLD, 26));
		button1.setForeground(SystemColor.activeCaptionText);
		button1.addActionListener(new ControlerSimulacaoPotenciaFundamental(Aeff, Veff, spinnerAmplitudeCorrente, spinnerAmplitudeTensao, spinnerAnguloTensao, spinnerAnguloCorrente, panelPotencia, panelTesao, panel, MaxDatePoints, textPotenciaAtiva,
				textPotenciaReativa, textPotenciaAparente, textFatorPotencia));
		button1.setBounds(354, 234, 319, 91);
		contentPane.add(button1);
		
		JLabel lbl180 = new JLabel(" -180º ≤ 0 ≤ 180º");
		lbl180.setForeground(Color.WHITE);
		lbl180.setBounds(35, 117, 141, 15);
		contentPane.add(lbl180);
		
		JLabel lbl220 = new JLabel(" 0 ≤ Vrms ≤ 220");
		lbl220.setForeground(Color.WHITE);
		lbl220.setBounds(219, 117, 132, 15);
		contentPane.add(lbl220);
		
		JLabel label = new JLabel(" 0 ≤ Vrms ≤ 100");
		label.setForeground(Color.WHITE);
		label.setBounds(706, 117, 132, 15);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel(" -180º ≤ 0 ≤ 180º");
		label_1.setForeground(Color.WHITE);
		label_1.setBounds(850, 117, 141, 15);
		contentPane.add(label_1);

	}

	public static void main(Object object) {
		// TODO Auto-generated method stub
		
	}

	public Object actionPerformed() {
		// TODO Auto-generated method stub
		return null;
	}
}

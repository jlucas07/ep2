package View;

import java.awt.*;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

import Controler.ControlerMenuPrincipal;

public class MenuPrincipal extends JFrame {

	private JPanel menu;
	private static final long serialVersionUID = 1L;
	private static final String TITULO = "ENERGIA ELÉTRICA - APRENDA QQE";
	
	private JLabel titulo;

	/**
	 * Launch the application.
	 */ 
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuPrincipal frame = new MenuPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MenuPrincipal() {
		
		setForeground(SystemColor.desktop);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle(TITULO);
		setBackground(Color.BLACK);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 723, 465);
		menu = new JPanel();
		menu.setBackground(Color.GRAY);
		menu.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(menu);
		menu.setLayout(null);
		
		JButton botao1 = new JButton("Fluxo de Potência Fundamental");
		botao1.setFont(new Font("Dialog", Font.BOLD, 26));
		botao1.setActionCommand("Fluxo de Potência Fundamental");
		botao1.addActionListener(new ControlerMenuPrincipal());
		
		botao1.setForeground(Color.BLACK);
		botao1.setBounds(118, 122, 508, 59);
		menu.add(botao1);
		
		JButton botao2 = new JButton("Distorção Harmônica");
		botao2.setForeground(Color.BLACK);
		botao2.setFont(new Font("DejaVu Sans", Font.BOLD, 28));
		botao2.setActionCommand("Distorção Harmônica");
		botao2.setBounds(118, 218, 508, 59);
		botao2.addActionListener(new ControlerMenuPrincipal());
		menu.add(botao2);
		
		JLabel opcao = new JLabel("Escolha uma das Opções:  ");
		opcao.setFont(new Font("Dialog", Font.BOLD, 35));
		opcao.setForeground(Color.GREEN);
		opcao.setBounds(39, 27, 533, 59);
		menu.add(opcao);
		
		
		titulo = new JLabel("",JLabel.CENTER);
	}
	
public void mostrarJanela ()
	
	{
		titulo.setText(TITULO);
		titulo.setFont(new Font ("Dialog", Font.PLAIN, 40));
		
	}
}

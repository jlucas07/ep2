package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

import Controler.ControlerDistorcaoHarmonica;
import Model.GraphPanel;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JSpinner;
import javax.swing.JButton;
import javax.swing.SpinnerNumberModel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import java.awt.Dimension;
import javax.swing.JProgressBar;
import javax.swing.DebugGraphics;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import java.awt.SystemColor;

public class DistorcaoHarmonica extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private JPanel contestePane;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DistorcaoHarmonica frame = new DistorcaoHarmonica();
					frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws IOException 
	 */
	public DistorcaoHarmonica() throws IOException {
		setBackground(Color.GRAY);
		setTitle("DISTORÇÃO HARMÔNICA");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1000, 5000);
		contestePane = new JPanel();
		contestePane.setBackground(Color.GRAY);
		contestePane.setBorder(new BevelBorder(BevelBorder.LOWERED, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE));
		contestePane.setLayout(null);
		
		JScrollPane contentPane = new JScrollPane(contestePane);
		contentPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		contentPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		getContentPane().add(contentPane, BorderLayout.CENTER);
		
		
		JLabel lblEntradas = new JLabel("Entradas");
		lblEntradas.setForeground(Color.WHITE);
		contestePane.add(lblEntradas);
		lblEntradas.setFont(new Font("Dialog", Font.BOLD, 25));
		lblEntradas.setBounds(461, 12, 176, 24);
		
		JLabel lblComponenteFundamental = new JLabel("Componente Fundamental");
		lblComponenteFundamental.setFont(new Font("Dialog", Font.BOLD, 20));
		lblComponenteFundamental.setForeground(Color.WHITE);
		lblComponenteFundamental.setBounds(30, 50, 310, 15);
		contestePane.add(lblComponenteFundamental);
		
		JLabel lblAmplitude = new JLabel("Amplitude");
		lblAmplitude.setForeground(Color.WHITE);
		lblAmplitude.setBounds(300, 70, 100, 15);
		contestePane.add(lblAmplitude);
		
		JProgressBar Amplitude = new JProgressBar();
		Amplitude.setMaximum(220);
		Amplitude.setOrientation(SwingConstants.VERTICAL);
		Amplitude.setDebugGraphicsOptions(DebugGraphics.NONE_OPTION);
		Amplitude.setAutoscrolls(true);
		Amplitude.setBounds(326, 100, 14, 50);
		contestePane.add(Amplitude);
		
		JLabel lblAnguloPrincipal = new JLabel("Ângulo de Fase ");
		lblAnguloPrincipal.setForeground(Color.WHITE);
		lblAnguloPrincipal.setBounds(460, 50, 198, 15);
		contestePane.add(lblAnguloPrincipal);
		
		JSpinner spinnerAnguloPrincipal = new JSpinner();
		spinnerAnguloPrincipal.setModel(new SpinnerNumberModel(0, -180, 180, 1));
		spinnerAnguloPrincipal.setBounds(460, 70, 150, 20);
		contestePane.add(spinnerAnguloPrincipal);
		
		JSpinner spinnerAmplitude = new JSpinner();
		spinnerAmplitude.setModel(new SpinnerNumberModel(0, 0, 220, 1));
		spinnerAmplitude.setBounds(460, 116, 150, 20);
		contestePane.add(spinnerAmplitude);
		
		JLabel lblNumHarmonicas = new JLabel("Número de Ordens Harmônicas: ");
		lblNumHarmonicas.setForeground(Color.WHITE);
		lblNumHarmonicas.setBounds(370, 159, 267, 15);
		contestePane.add(lblNumHarmonicas);
		
		JSpinner spinnerHarmonicas = new JSpinner();
		spinnerHarmonicas.setModel(new SpinnerNumberModel(0, 0, 5, 1));
		spinnerHarmonicas.setBounds(412, 186, 150, 20);
		contestePane.add(spinnerHarmonicas);
		
		JLabel lblHarmonicas = new JLabel("Harmônicos");
		lblHarmonicas.setForeground(Color.WHITE);
		lblHarmonicas.setBounds(649, 159, 92, 15);
		contestePane.add(lblHarmonicas);
		
		JRadioButton imp = new JRadioButton("Ímpares");
		imp.setBackground(Color.GRAY);
		imp.setForeground(Color.WHITE);
		imp.setBounds(649, 184, 100, 23);
		contestePane.add(imp);
		
		JRadioButton par = new JRadioButton("Pares");
		par.setForeground(Color.WHITE);
		par.setBackground(Color.GRAY);
		par.setFont(new Font("Dialog", Font.BOLD, 12));
		par.setBounds(649, 211, 100, 23);
		contestePane.add(par);
		
		JLabel lblSaida = new JLabel("Saídas");
		lblSaida.setForeground(Color.WHITE);
		lblSaida.setFont(new Font("DejaVu Serif Condensed", Font.BOLD, 25));
		lblSaida.setBounds(482, 264, 176, 24);
		contestePane.add(lblSaida);
		
		JLabel lblResultado = new JLabel("Resultado");
		lblResultado.setForeground(Color.WHITE);
		lblResultado.setHorizontalAlignment(SwingConstants.CENTER);
		lblResultado.setBounds(50, 300, 198, 15);
		contestePane.add(lblResultado);
		
		JLabel lblSerieFourier = new JLabel("Série de Fourier Amplitude-Fase");
		lblSerieFourier.setForeground(Color.WHITE);
		lblSerieFourier.setBounds(400, 300, 267, 15);
		contestePane.add(lblSerieFourier);
		
		JLabel lblRes = new JLabel("Resulted");
		lblRes.setForeground(Color.WHITE);
		lblRes.setFont(new Font("Dialog", Font.BOLD, 10));
		lblRes.setVerticalAlignment(SwingConstants.TOP);
		lblRes.setBounds(400, 340, 800, 15);
		contestePane.add(lblRes);
		
		// ONDA 1
		
		JLabel lblAmplu1 = new JLabel("Amplitude ");
		lblAmplu1.setForeground(Color.WHITE);
		lblAmplu1.setBounds(410, 305, 198, 15);
		lblAmplu1.setVisible(false);
		contestePane.add(lblAmplu1);
		
		JSpinner spinnerAmplitude1 = new JSpinner();
		spinnerAmplitude1.setModel(new SpinnerNumberModel(0, 0, 220, 1));
		spinnerAmplitude1.setBounds(410, 320, 150, 20);
		spinnerAmplitude1.setVisible(false);
		contestePane.add(spinnerAmplitude1);
		
		JLabel lblOrdem1 = new JLabel("Ordem Harmônica");
		lblOrdem1.setForeground(Color.WHITE);
		lblOrdem1.setBounds(605, 250, 170, 15);
		lblOrdem1.setVisible(false);
		contestePane.add(lblOrdem1);
		
		JSpinner spinnerOrdem1 = new JSpinner();
		spinnerOrdem1.setBounds(605, 280, 150, 20);
		spinnerOrdem1.setModel(new SpinnerNumberModel(0, 0, 15, 1));
		spinnerOrdem1.setVisible(false);
		contestePane.add(spinnerOrdem1);
		
		JLabel lblAmplitude1 = new JLabel("Amplitude");
		lblAmplitude1.setForeground(Color.WHITE);
		lblAmplitude1.setBounds(300, 240, 100, 15);
		lblAmplitude1.setVisible(false);
		contestePane.add(lblAmplitude1);
		
		JProgressBar Amplitude1 = new JProgressBar();
		Amplitude1.setMaximum(220);
		Amplitude1.setOrientation(SwingConstants.VERTICAL);
		Amplitude1.setDebugGraphicsOptions(DebugGraphics.NONE_OPTION);
		Amplitude1.setAutoscrolls(true);
		Amplitude1.setBounds(326, 270, 14, 50);
		Amplitude1.setVisible(false);
		contestePane.add(Amplitude1);
		
		JLabel lblAngulo1 = new JLabel("Ângulo de Fase ");
		lblAngulo1.setForeground(Color.WHITE);
		lblAngulo1.setBounds(410, 250, 198, 15);
		lblAngulo1.setVisible(false);
		contestePane.add(lblAngulo1);
		
		JSpinner spinnerAngulo1 = new JSpinner();
		spinnerAngulo1.setBounds(410, 280, 150, 20);
		spinnerAngulo1.setModel(new SpinnerNumberModel(0, -180, 180, 1));
		spinnerAngulo1.setVisible(false);
		contestePane.add(spinnerAngulo1);
		
		// ONDA 2
		
		JLabel lblAmplu2 = new JLabel("Amplitude ");
		lblAmplu2.setForeground(Color.WHITE);
		lblAmplu2.setBounds(410, 435, 198, 15);
		lblAmplu2.setVisible(false);
		contestePane.add(lblAmplu2);
		
		JSpinner spinnerAmplitude2 = new JSpinner();
		spinnerAmplitude2.setModel(new SpinnerNumberModel(0, 0, 220, 1));
		spinnerAmplitude2.setBounds(410, 450, 150, 20);
		spinnerAmplitude2.setVisible(false);
		contestePane.add(spinnerAmplitude2);
		
		JLabel lblOrdem2 = new JLabel("Ordem Harmônica");
		lblOrdem2.setForeground(Color.WHITE);
		lblOrdem2.setBounds(605, 380, 170, 15);
		lblOrdem2.setVisible(false);
		contestePane.add(lblOrdem2);
		
		JSpinner spinnerOrdem2 = new JSpinner();
		spinnerOrdem2.setBounds(605, 410, 150, 20);
		spinnerOrdem2.setModel(new SpinnerNumberModel(0, 0, 15, 1));
		spinnerOrdem2.setVisible(false);
		contestePane.add(spinnerOrdem2);
		
		JLabel lblAmplitude2 = new JLabel("Amplitude");
		lblAmplitude2.setForeground(Color.WHITE);
		lblAmplitude2.setBounds(300, 370, 100, 15);
		lblAmplitude2.setVisible(false);
		contestePane.add(lblAmplitude2);
		
		JProgressBar Amplitude2 = new JProgressBar();
		Amplitude2.setMaximum(220);
		Amplitude2.setOrientation(SwingConstants.VERTICAL);
		Amplitude2.setDebugGraphicsOptions(DebugGraphics.NONE_OPTION);
		Amplitude2.setAutoscrolls(true);
		Amplitude2.setBounds(326, 400, 14, 50);
		Amplitude2.setVisible(false);
		contestePane.add(Amplitude2);
		
		JLabel lblAngulo2 = new JLabel("Ângulo de Fase ");
		lblAngulo2.setForeground(Color.WHITE);
		lblAngulo2.setBounds(410, 380, 198, 15);
		lblAngulo2.setVisible(false);
		contestePane.add(lblAngulo2);
		
		JSpinner spinnerAngulo2 = new JSpinner();
		spinnerAngulo2.setBounds(410, 410, 150, 20);
		spinnerAngulo2.setModel(new SpinnerNumberModel(0, -180, 180, 1));
		spinnerAngulo2.setVisible(false);
		contestePane.add(spinnerAngulo2);
		
		// ONDA 3
		
		JLabel lblAmplu3 = new JLabel("Amplitude ");
		lblAmplu3.setForeground(Color.WHITE);
		lblAmplu3.setBounds(410, 565, 198, 15);
		lblAmplu3.setVisible(false);
		contestePane.add(lblAmplu3);
		
		JSpinner spinnerAmplitude3 = new JSpinner();
		spinnerAmplitude3.setModel(new SpinnerNumberModel(0, 0, 220, 1));
		spinnerAmplitude3.setBounds(410, 580, 150, 20);
		spinnerAmplitude3.setVisible(false);
		contestePane.add(spinnerAmplitude3);
		
		JLabel lblOrdem3 = new JLabel("Ordem Harmônica");
		lblOrdem3.setForeground(Color.WHITE);
		lblOrdem3.setBounds(605, 510, 170, 15);
		lblOrdem3.setVisible(false);
		contestePane.add(lblOrdem3);
		
		JSpinner spinnerOrdem3 = new JSpinner();
		spinnerOrdem3.setBounds(605, 540, 150, 20);
		spinnerOrdem3.setModel(new SpinnerNumberModel(0, 0, 15, 1));
		spinnerOrdem3.setVisible(false);
		contestePane.add(spinnerOrdem3);
		
		JLabel lblAmplitude3 = new JLabel("Amplitude");
		lblAmplitude3.setForeground(Color.WHITE);
		lblAmplitude3.setBounds(300, 500, 100, 15);
		lblAmplitude3.setVisible(false);
		contestePane.add(lblAmplitude3);
		
		JProgressBar Amplitude3 = new JProgressBar();
		Amplitude3.setMaximum(220);
		Amplitude3.setOrientation(SwingConstants.VERTICAL);
		Amplitude3.setDebugGraphicsOptions(DebugGraphics.NONE_OPTION);
		Amplitude3.setAutoscrolls(true);
		Amplitude3.setBounds(326, 530, 14, 50);
		Amplitude3.setVisible(false);
		contestePane.add(Amplitude3);
		
		JLabel lblAngulo3 = new JLabel("Ângulo de Fase ");
		lblAngulo3.setForeground(Color.WHITE);
		lblAngulo3.setBounds(410, 510, 198, 15);
		lblAngulo3.setVisible(false);
		contestePane.add(lblAngulo3);
		
		JSpinner spinnerAngulo3 = new JSpinner();
		spinnerAngulo3.setBounds(410, 540, 150, 20);
		spinnerAngulo3.setModel(new SpinnerNumberModel(0, -180, 180, 1));
		spinnerAngulo3.setVisible(false);
		contestePane.add(spinnerAngulo3);
		
		// ONDA 4
		
		JLabel lblAmplu4 = new JLabel("Amplitude ");
		lblAmplu4.setForeground(Color.WHITE);
		lblAmplu4.setBounds(410, 695, 198, 15);
		lblAmplu4.setVisible(false);
		contestePane.add(lblAmplu4);
		
		JSpinner spinnerAmplitude4 = new JSpinner();
		spinnerAmplitude4.setModel(new SpinnerNumberModel(0, 0, 220, 1));
		spinnerAmplitude4.setBounds(410, 710, 150, 20);
		spinnerAmplitude4.setVisible(false);
		contestePane.add(spinnerAmplitude4);
				
		JLabel lblOrdem4 = new JLabel("Ordem Harmônica");
		lblOrdem4.setForeground(Color.WHITE);
		lblOrdem4.setBounds(605, 640, 170, 15);
		lblOrdem4.setVisible(false);
		contestePane.add(lblOrdem4);
				
		JSpinner spinnerOrdem4 = new JSpinner();
		spinnerOrdem4.setBounds(605, 670, 150, 20);
		spinnerOrdem4.setModel(new SpinnerNumberModel(0, 0, 15, 1));
		spinnerOrdem4.setVisible(false);
		contestePane.add(spinnerOrdem4);
		
		JLabel lblAmplitude4 = new JLabel("Amplitude");
		lblAmplitude4.setForeground(Color.WHITE);
		lblAmplitude4.setBounds(300, 630, 100, 15);
		lblAmplitude4.setVisible(false);
		contestePane.add(lblAmplitude4);
		
		JProgressBar Amplitude4 = new JProgressBar();
		Amplitude4.setMaximum(220);
		Amplitude4.setOrientation(SwingConstants.VERTICAL);
		Amplitude4.setDebugGraphicsOptions(DebugGraphics.NONE_OPTION);
		Amplitude4.setAutoscrolls(true);
		Amplitude4.setBounds(326, 660, 14, 50);
		Amplitude4.setVisible(false);
		contestePane.add(Amplitude4);
		
		JLabel lblAngulo4 = new JLabel("Ângulo de Fase ");
		lblAngulo4.setForeground(Color.WHITE);
		lblAngulo4.setBounds(410, 640, 198, 15);
		lblAngulo4.setVisible(false);
		contestePane.add(lblAngulo4);
				
		JSpinner spinnerAngulo4 = new JSpinner();
		spinnerAngulo4.setBounds(410, 670, 150, 20);
		spinnerAngulo4.setModel(new SpinnerNumberModel(0, -180, 180, 1));
		spinnerAngulo4.setVisible(false);
		contestePane.add(spinnerAngulo4);
		
		// ONDA 5
		
		JLabel lblAmplu5 = new JLabel("Amplitude ");
		lblAmplu5.setForeground(Color.WHITE);
		lblAmplu5.setBounds(410, 825, 198, 15);
		lblAmplu5.setVisible(false);
		contestePane.add(lblAmplu5);
		
		JSpinner spinnerAmplitude5 = new JSpinner();
		spinnerAmplitude5.setModel(new SpinnerNumberModel(0, 0, 220, 1));
		spinnerAmplitude5.setBounds(410, 840, 150, 20);
		spinnerAmplitude5.setVisible(false);
		contestePane.add(spinnerAmplitude5);
						
		JLabel lblOrdem5 = new JLabel("Ordem Harmônica");
		lblOrdem5.setForeground(Color.WHITE);
		lblOrdem5.setBounds(605, 770, 170, 15);
		lblOrdem5.setVisible(false);
		contestePane.add(lblOrdem5);
						
		JSpinner spinnerOrdem5 = new JSpinner();
		spinnerOrdem5.setBounds(605, 800, 150, 20);
		spinnerOrdem5.setModel(new SpinnerNumberModel(0, 0, 15, 1));
		spinnerOrdem5.setVisible(false);
		contestePane.add(spinnerOrdem5);
		
		JLabel lblAmplitude5 = new JLabel("Amplitude");
		lblAmplitude5.setForeground(Color.WHITE);
		lblAmplitude5.setBounds(300, 760, 100, 15);
		lblAmplitude5.setVisible(false);
		contestePane.add(lblAmplitude5);
		
		JProgressBar Amplitude5 = new JProgressBar();
		Amplitude5.setMaximum(220);
		Amplitude5.setOrientation(SwingConstants.VERTICAL);
		Amplitude5.setDebugGraphicsOptions(DebugGraphics.NONE_OPTION);
		Amplitude5.setAutoscrolls(true);
		Amplitude5.setBounds(326, 790, 14, 50);
		Amplitude5.setVisible(false);
		contestePane.add(Amplitude5);
		
		JLabel lblAngulo5 = new JLabel("Ângulo de Fase");
		lblAngulo5.setForeground(Color.WHITE);
		lblAngulo5.setBounds(410, 770, 198, 15);
		lblAngulo5.setVisible(false);
		contestePane.add(lblAngulo5);
						
		JSpinner spinnerAngulo5 = new JSpinner();
		spinnerAngulo5.setBounds(410, 800, 150, 20);
		spinnerAngulo5.setModel(new SpinnerNumberModel(0, -180, 180, 1));
		spinnerAngulo5.setVisible(false);
		contestePane.add(spinnerAngulo5);
		
		
		List<Double> scores = new ArrayList<>();
		int MaxDatePoints = 50;
		GraphPanel panel = new Model.GraphPanel(scores);
		panel.setOpaque(false);
		panel.setBorder(null);
		panel.setBounds(682,12,300,175);
		panel.setBackground(new Color(238, 238, 238));
		contestePane.add(panel);
		
		List<Double> scores2 = new ArrayList<>();
		GraphPanel panel2 = new Model.GraphPanel(scores2);
		panel2.setOpaque(false);
		panel2.setBorder(null);
		panel2.setBackground(new Color(238, 238, 238));
		panel2.setBounds(10,220,300,175);
		panel2.setPreferredSize(new Dimension(800, 600));
		panel2.setVisible(false);
		contestePane.add(panel2);
		
		List<Double> scores3 = new ArrayList<>();
		GraphPanel panel3 = new Model.GraphPanel(scores3);
		panel3.setOpaque(false);
		panel3.setBorder(null);
		panel3.setBackground(new Color(238, 238, 238));
		panel3.setBounds(10,350,300,175);
		panel3.setPreferredSize(new Dimension(800, 600));
		panel3.setVisible(false);
		contestePane.add(panel3);
		
		List<Double> scores4 = new ArrayList<>();
		GraphPanel panel4 = new Model.GraphPanel(scores4);
		panel4.setOpaque(false);
		panel4.setBorder(null);
		panel4.setBackground(new Color(238, 238, 238));
		panel4.setBounds(10,480,300,175);
		panel4.setPreferredSize(new Dimension(800, 600));
		panel4.setVisible(false);
		contestePane.add(panel4);
		
		List<Double> scores5 = new ArrayList<>();
		GraphPanel panel5 = new Model.GraphPanel(scores5);
		panel5.setOpaque(false);
		panel5.setBorder(null);
		panel5.setBackground(new Color(238, 238, 238));
		panel5.setBounds(10,610,300,175);
		panel5.setPreferredSize(new Dimension(800, 600));
		panel5.setVisible(false);
		contestePane.add(panel5);
		
		List<Double> scores6 = new ArrayList<>();
		GraphPanel panel6 = new Model.GraphPanel(scores6);
		panel6.setOpaque(false);
		panel6.setBorder(null);
		panel6.setBackground(new Color(238, 238, 238));
		panel6.setBounds(10,740,300,175);
		panel6.setPreferredSize(new Dimension(800, 600));
		panel6.setVisible(false);
		contestePane.add(panel6);
		
		List<Double> scores7 = new ArrayList<>();
		GraphPanel panel7 = new Model.GraphPanel(scores7);
		panel7.setOpaque(false);
		panel7.setBorder(null);
		panel7.setBackground(new Color(238, 238, 238));
		panel7.setBounds(10,300,300,175);
		panel7.setPreferredSize(new Dimension(800, 600));
		contestePane.add(panel7);
		
		JLabel lblAmplu = new JLabel("Amplitude");
		lblAmplu.setForeground(Color.WHITE);
		lblAmplu.setBounds(470, 99, 100, 15);
		contestePane.add(lblAmplu);

		
		JButton button = new JButton("SIMULAR/CALCULAR");
		button.setForeground(SystemColor.activeCaptionText);
		button.setFont(new Font("DejaVu Sans", Font.BOLD, 15));
		button.setBounds(50, 100, 223, 74);
		button.addActionListener(new ControlerDistorcaoHarmonica(Amplitude,
		   												Amplitude1,
		   												Amplitude2,
		   												Amplitude3,
		   												Amplitude4,
		   												Amplitude5,
		   												panel,
		   												panel2,
		   												panel3,
		   												panel4,
		   												panel5,
		   												panel6,
		   												panel7,
		   												MaxDatePoints,
		   												spinnerAnguloPrincipal,
		   												spinnerAngulo1,
		   												spinnerAngulo2,
		   												spinnerAngulo3,
		   												spinnerAngulo4,
		   												spinnerAngulo5,
		   												spinnerAmplitude,
		   												spinnerAmplitude1,
		   												spinnerAmplitude2,
		   												spinnerAmplitude3,
		   												spinnerAmplitude4,
		   												spinnerAmplitude5,
		   												spinnerOrdem1,
		   												spinnerOrdem2,
		   												spinnerOrdem3,
		   												spinnerOrdem4,
		   												spinnerOrdem5,
		   												spinnerHarmonicas,
		   												lblOrdem1,
		   												lblOrdem2,
		   												lblOrdem3,
		   												lblOrdem4,
		   												lblOrdem5,
		   												lblAmplitude1,
		   												lblAmplitude2,
		   												lblAmplitude3,
		   												lblAmplitude4,
		   												lblAmplitude5,
		   												lblAngulo1,
		   												lblAngulo2,
		   												lblAngulo3,
		   												lblAngulo4,
		   												lblAngulo5,
		   												lblAmplu1,
		   												lblAmplu2,
		   												lblAmplu3,
		   												lblAmplu4,
		   												lblAmplu5,
		   												lblSaida,
		   												lblResultado,
		   												lblSerieFourier,
		   												lblRes));
		contestePane.add(button);
			
		}

	}

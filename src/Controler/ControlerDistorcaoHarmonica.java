package Controler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.Math;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JSpinner;

import Model.GraphPanel;

public class ControlerDistorcaoHarmonica implements ActionListener {
	
	private JProgressBar Amplitude;
	private JProgressBar Amplitude1;
	private JProgressBar Amplitude2;
	private JProgressBar Amplitude3;
	private JProgressBar Amplitude4;
	private JProgressBar Amplitude5;
	
	private GraphPanel panel;
	private GraphPanel panel2;
	private GraphPanel panel3;
	private GraphPanel panel4;
	private GraphPanel panel5;
	private GraphPanel panel6;
	private GraphPanel panel7;
	
	private int MaxDatePoints;
	
	private JSpinner spinnerAnguloPrincipal;
	private JSpinner spinnerAngulo1;
	private JSpinner spinnerAngulo2;
	private JSpinner spinnerAngulo3;
	private JSpinner spinnerAngulo4;
	private JSpinner spinnerAngulo5;
	private JSpinner spinnerAmplitude;
	private JSpinner spinnerAmplitude1;
	private JSpinner spinnerAmplitude2;
	private JSpinner spinnerAmplitude3;
	private JSpinner spinnerAmplitude4;
	private JSpinner spinnerAmplitude5;
	private JSpinner spinnerOrdem1;
	private JSpinner spinnerOrdem2;
	private JSpinner spinnerOrdem3;
	private JSpinner spinnerOrdem4;
	private JSpinner spinnerOrdem5;
	private JSpinner spinnerHarmonicas;
	
	private JLabel lblOrdem1;
	private JLabel lblOrdem2;
	private JLabel lblOrdem3;
	private JLabel lblOrdem4;
	private JLabel lblOrdem5;
	private JLabel lblAmplitude1;
	private JLabel lblAmplitude2;
	private JLabel lblAmplitude3;
	private JLabel lblAmplitude4;
	private JLabel lblAmplitude5;
	private JLabel lblAmplu1;
	private JLabel lblAmplu2;
	private JLabel lblAmplu3;
	private JLabel lblAmplu4;
	private JLabel lblAmplu5;
	private JLabel lblAngulo1;
	private JLabel lblAngulo2;
	private JLabel lblAngulo3;
	private JLabel lblAngulo4;
	private JLabel lblAngulo5;
	private JLabel lblSaida;
	private JLabel lblResultado;
	private JLabel lblSerieFourier;
	private JLabel lblRes;
	

	public ControlerDistorcaoHarmonica (JProgressBar Amplitude,
									   		JProgressBar Amplitude1,
									   		JProgressBar Amplitude2,
									   		JProgressBar Amplitude3,
									   		JProgressBar Amplitude4,
									   		JProgressBar Amplitude5,
									   		
									   		GraphPanel panel,
									   		GraphPanel panel2,
									   		GraphPanel panel3,
									   		GraphPanel panel4,
									   		GraphPanel panel5,
									   		GraphPanel panel6,
									   		GraphPanel panel7,
									   		
										   	int MaxDatePoints,
										   	
										   	JSpinner spinnerAnguloPrincipal,
										   	JSpinner spinnerAngulo1,
										   	JSpinner spinnerAngulo2,
										   	JSpinner spinnerAngulo3,
										   	JSpinner spinnerAngulo4,
										   	JSpinner spinnerAngulo5,
										   	JSpinner spinnerAmplitude,
										   	JSpinner spinnerAmplitude1,
										   	JSpinner spinnerAmplitude2,
										   	JSpinner spinnerAmplitude3,
										   	JSpinner spinnerAmplitude4,
										   	JSpinner spinnerAmplitude5,
										   	JSpinner spinnerOrdem1,
										   	JSpinner spinnerOrdem2,
										   	JSpinner spinnerOrdem3,
										   	JSpinner spinnerOrdem4,
										   	JSpinner spinnerOrdem5,
										   	JSpinner spinnerHarmonicas,
										   	
										   	JLabel lblOrdem1,
										   	JLabel lblOrdem2,
										   	JLabel lblOrdem3,
										   	JLabel lblOrdem4,
										   	JLabel lblOrdem5,
										   	JLabel lblAmplitude1,
										   	JLabel lblAmplitude2,
										   	JLabel lblAmplitude3,
										   	JLabel lblAmplitude4,
										   	JLabel lblAmplitude5,
										   	JLabel lblAngulo1,
										   	JLabel lblAngulo2,
										   	JLabel lblAngulo3,
										   	JLabel lblAngulo4,
										   	JLabel lblAngulo5,
										   	JLabel lblAmplu1,
										   	JLabel lblAmplu2,
										   	JLabel lblAmplu3,
										   	JLabel lblAmplu4,
										   	JLabel lblAmplu5,
										   	JLabel lblSaida,
										   	JLabel lblResultado,
										   	JLabel lblSerieFourier,
										   	JLabel lblRes) throws IOException {
		
											this.Amplitude = Amplitude;
											this.Amplitude1 = Amplitude1;
											this.Amplitude2 = Amplitude2;
											this.Amplitude3 = Amplitude3;
											this.Amplitude4 = Amplitude4;
											this.Amplitude5 = Amplitude5;
		
											this.panel = panel;
											this.panel2 = panel2;
											this.panel3 = panel3;
											this.panel4 = panel4;
											this.panel5 = panel5;
											this.panel6 = panel6;
											this.panel7 = panel7;
		
											this.MaxDatePoints = MaxDatePoints;
		
											this.spinnerAnguloPrincipal = spinnerAnguloPrincipal;
											this.spinnerAngulo1 = spinnerAngulo1;
											this.spinnerAngulo2 = spinnerAngulo2;
											this.spinnerAngulo3 = spinnerAngulo3;
											this.spinnerAngulo4 = spinnerAngulo4;
											this.spinnerAngulo5 = spinnerAngulo5;
											this.spinnerAmplitude = spinnerAmplitude;
											this.spinnerAmplitude1 = spinnerAmplitude1;
											this.spinnerAmplitude2 = spinnerAmplitude2;
											this.spinnerAmplitude3 = spinnerAmplitude3;
											this.spinnerAmplitude4 = spinnerAmplitude4;
											this.spinnerAmplitude5 = spinnerAmplitude5;
											this.spinnerOrdem1 = spinnerOrdem1;
											this.spinnerOrdem2 = spinnerOrdem2;
											this.spinnerOrdem3 = spinnerOrdem3;
											this.spinnerOrdem4 = spinnerOrdem4;
											this.spinnerOrdem5 = spinnerOrdem5;
											this.spinnerHarmonicas = spinnerHarmonicas;
		
											this.lblOrdem1 = lblOrdem1;
											this.lblOrdem2 = lblOrdem2;
											this.lblOrdem3 = lblOrdem3;
											this.lblOrdem4 = lblOrdem4;
											this.lblOrdem5 = lblOrdem5;
											this.lblAmplitude1 = lblAmplitude1;
											this.lblAmplitude2 = lblAmplitude2;
											this.lblAmplitude3 = lblAmplitude3;
											this.lblAmplitude4 = lblAmplitude4;
											this.lblAmplitude5 = lblAmplitude5;
											this.lblAmplu1 = lblAmplu1;
											this.lblAmplu2 = lblAmplu2;
											this.lblAmplu3 = lblAmplu3;
											this.lblAmplu4 = lblAmplu4;
											this.lblAmplu5 = lblAmplu5;
											this.lblAngulo1 = lblAngulo1;
											this.lblAngulo2 = lblAngulo2;
											this.lblAngulo3 = lblAngulo3;
											this.lblAngulo4 = lblAngulo4;
											this.lblAngulo5 = lblAngulo5;
											this.lblSaida = lblSaida;
											this.lblResultado = lblResultado;
											this.lblSerieFourier = lblSerieFourier;
											this.lblRes = lblRes;
	}

	public void actionPerformed(ActionEvent arg0) {
		
		try {
			
			Amplitude.setValue((Integer)spinnerAmplitude.getValue());
			Amplitude1.setValue((Integer)spinnerAmplitude1.getValue());
			Amplitude2.setValue((Integer)spinnerAmplitude2.getValue());
			Amplitude3.setValue((Integer)spinnerAmplitude3.getValue());
			Amplitude4.setValue((Integer)spinnerAmplitude4.getValue());
			Amplitude5.setValue((Integer)spinnerAmplitude5.getValue());
			
			List<Double> scores = new ArrayList<>();
			for (int i = 0; i < MaxDatePoints; i++) {
		          scores.add((double)Amplitude.getValue()*Math.cos(Math.toRadians((2*Math.PI*60*i+(Integer)spinnerAnguloPrincipal.getValue()))));
		        }
			panel.setScores(scores);
			
			List<Double> scores2 = new ArrayList<>();
			for (int i = 0; i < MaxDatePoints; i++) {
		          scores2.add((double)Amplitude1.getValue()*Math.cos(Math.toRadians(((Integer)(spinnerOrdem1.getValue())*2*Math.PI*60*i+(Integer)spinnerAngulo1.getValue()))));
		        }
			panel2.setScores(scores2);
			
			List<Double> scores3 = new ArrayList<>();
			for (int i = 0; i < MaxDatePoints; i++) {
		          scores3.add((double)Amplitude2.getValue()*Math.cos(Math.toRadians(((Integer)(spinnerOrdem2.getValue())*2*Math.PI*60*i+(Integer)spinnerAngulo2.getValue()))));
		        }
			panel3.setScores(scores3);
			
			List<Double> scores4 = new ArrayList<>();
			for (int i = 0; i < MaxDatePoints; i++) {
		          scores4.add((double)Amplitude3.getValue()*Math.cos(Math.toRadians(((Integer)(spinnerOrdem3.getValue())*2*Math.PI*60*i+(Integer)spinnerAngulo3.getValue()))));
		        }
			panel4.setScores(scores4);
			
			List<Double> scores5 = new ArrayList<>();
			for (int i = 0; i < MaxDatePoints; i++) {
		          scores5.add((double)Amplitude4.getValue()*Math.cos(Math.toRadians(((Integer)(spinnerOrdem4.getValue())*2*Math.PI*60*i+(Integer)spinnerAngulo4.getValue()))));
		        }
			panel5.setScores(scores5);
			
			List<Double> scores6 = new ArrayList<>();
			for (int i = 0; i < MaxDatePoints; i++) {
		          scores6.add((double)Amplitude5.getValue()*Math.cos(Math.toRadians(((Integer)(spinnerOrdem5.getValue())*2*Math.PI*60*i+(Integer)spinnerAngulo5.getValue()))));
		        }
			panel6.setScores(scores6);
			
			List<Double> scores7 = new ArrayList<>();
			if((Integer)spinnerHarmonicas.getValue() == 0) {
				for (int i = 0; i < MaxDatePoints; i++) {
			          scores7.add((double)Amplitude.getValue()*Math.cos(Math.toRadians((2*Math.PI*60*i+(Integer)spinnerAnguloPrincipal.getValue()))));
			        }
				panel7.setScores(scores7);
			}
			else if((Integer)spinnerHarmonicas.getValue() == 1) {
				for (int i = 0; i < MaxDatePoints; i++) {
			          scores7.add((double)Amplitude.getValue()*Math.cos(Math.toRadians((2*Math.PI*60*i+(Integer)spinnerAnguloPrincipal.getValue())))+
			        		  		(double)Amplitude1.getValue()*Math.cos(Math.toRadians(((Integer)(spinnerOrdem1.getValue())*2*Math.PI*60*i+(Integer)spinnerAngulo1.getValue()))));
			        }
				panel7.setScores(scores7);
			}
			else if((Integer)spinnerHarmonicas.getValue() == 2) {
				for (int i = 0; i < MaxDatePoints; i++) {
			          scores7.add((double)Amplitude.getValue()*Math.cos(Math.toRadians((2*Math.PI*60*i+(Integer)spinnerAnguloPrincipal.getValue())))+
			        		  		(double)Amplitude1.getValue()*Math.cos(Math.toRadians(((Integer)(spinnerOrdem1.getValue())*2*Math.PI*60*i+(Integer)spinnerAngulo1.getValue())))+
			        		  		(double)Amplitude2.getValue()*Math.cos(Math.toRadians(((Integer)(spinnerOrdem2.getValue())*2*Math.PI*60*i+(Integer)spinnerAngulo2.getValue()))));
			        }
				panel7.setScores(scores7);
				}
			else if((Integer)spinnerHarmonicas.getValue() == 3) {
				for (int i = 0; i < MaxDatePoints; i++) {
			          scores7.add((double)Amplitude.getValue()*Math.cos(Math.toRadians((2*Math.PI*60*i+(Integer)spinnerAnguloPrincipal.getValue())))+
			        		  		(double)Amplitude1.getValue()*Math.cos(Math.toRadians(((Integer)(spinnerOrdem1.getValue())*2*Math.PI*60*i+(Integer)spinnerAngulo1.getValue())))+
			        		  		(double)Amplitude2.getValue()*Math.cos(Math.toRadians(((Integer)(spinnerOrdem2.getValue())*2*Math.PI*60*i+(Integer)spinnerAngulo2.getValue())))+
			        		  		(double)Amplitude3.getValue()*Math.cos(Math.toRadians(((Integer)(spinnerOrdem3.getValue())*2*Math.PI*60*i+(Integer)spinnerAngulo3.getValue()))));
			        }
				panel7.setScores(scores7);
				}
			else if((Integer)spinnerHarmonicas.getValue() == 4) {
				for (int i = 0; i < MaxDatePoints; i++) {
			          scores7.add((double)Amplitude.getValue()*Math.cos(Math.toRadians((2*Math.PI*60*i+(Integer)spinnerAnguloPrincipal.getValue())))+
			        		  		(double)Amplitude1.getValue()*Math.cos(Math.toRadians(((Integer)(spinnerOrdem1.getValue())*2*Math.PI*60*i+(Integer)spinnerAngulo1.getValue())))+
			        		  		(double)Amplitude2.getValue()*Math.cos(Math.toRadians(((Integer)(spinnerOrdem2.getValue())*2*Math.PI*60*i+(Integer)spinnerAngulo2.getValue())))+
			        		  		(double)Amplitude3.getValue()*Math.cos(Math.toRadians(((Integer)(spinnerOrdem3.getValue())*2*Math.PI*60*i+(Integer)spinnerAngulo3.getValue())))+
			        		  		(double)Amplitude4.getValue()*Math.cos(Math.toRadians(((Integer)(spinnerOrdem4.getValue())*2*Math.PI*60*i+(Integer)spinnerAngulo4.getValue()))));
			        }
				panel7.setScores(scores7);
				}
			
			else if((Integer)spinnerHarmonicas.getValue() == 5) {
			for (int i = 0; i < MaxDatePoints; i++) {
		          scores7.add((double)Amplitude.getValue()*Math.cos(Math.toRadians((2*Math.PI*60*i+(Integer)spinnerAnguloPrincipal.getValue())))+
		        		  		(double)Amplitude1.getValue()*Math.cos(Math.toRadians(((Integer)(spinnerOrdem1.getValue())*2*Math.PI*60*i+(Integer)spinnerAngulo1.getValue())))+
		        		  		(double)Amplitude2.getValue()*Math.cos(Math.toRadians(((Integer)(spinnerOrdem2.getValue())*2*Math.PI*60*i+(Integer)spinnerAngulo2.getValue())))+
		        		  		(double)Amplitude3.getValue()*Math.cos(Math.toRadians(((Integer)(spinnerOrdem3.getValue())*2*Math.PI*60*i+(Integer)spinnerAngulo3.getValue())))+
		        		  		(double)Amplitude4.getValue()*Math.cos(Math.toRadians(((Integer)(spinnerOrdem4.getValue())*2*Math.PI*60*i+(Integer)spinnerAngulo4.getValue())))+
		        		  		(double)Amplitude5.getValue()*Math.cos(Math.toRadians(((Integer)(spinnerOrdem5.getValue())*2*Math.PI*60*i+(Integer)spinnerAngulo5.getValue()))));
		        }
			panel7.setScores(scores7);
			}
			if((Integer)spinnerHarmonicas.getValue() == 0) {
				lblRes.setText(String.valueOf("f(t)="+Amplitude.getValue()+"cos(wt)"));
				}
			else if((Integer)spinnerHarmonicas.getValue() == 1) {
				lblRes.setText(String.valueOf("f(t)="+Amplitude.getValue()+"cos(wt)"+'+'+Amplitude1.getValue()+"cos("+spinnerOrdem1.getValue()+"wt"+'+'+spinnerAngulo1.getValue()+"°)"));
				}
			else if((Integer)spinnerHarmonicas.getValue() == 2) {
				lblRes.setText(String.valueOf("f(t)="+Amplitude.getValue()+"cos(wt)"+'+'+Amplitude1.getValue()+"cos("+spinnerOrdem1.getValue()+"wt"+'+'+spinnerAngulo1.getValue()+"°)"+'+'+Amplitude2.getValue()+"cos("+spinnerOrdem2.getValue()+"wt"+'+'+spinnerAngulo2.getValue()+"°)"));
				}
			else if((Integer)spinnerHarmonicas.getValue() == 3) {
				lblRes.setText(String.valueOf("f(t)="+Amplitude.getValue()+"+"+"cos(wt)"+'+'+Amplitude1.getValue()+"cos("+spinnerOrdem1.getValue()+"wt)"+'+'+Amplitude2.getValue()+"cos("+spinnerOrdem2.getValue()+"wt"+'+'+spinnerAngulo2.getValue()+"°)"+'+'+Amplitude3.getValue()+"cos("+spinnerOrdem3.getValue()+"wt"+'+'+spinnerAngulo3.getValue()+"°)"));
				}
			else if((Integer)spinnerHarmonicas.getValue() == 4) {
				lblRes.setText(String.valueOf("f(t)="+Amplitude.getValue()+"+"+"cos(wt)"+'+'+Amplitude1.getValue()+"cos("+spinnerOrdem1.getValue()+"wt)"+'+'+Amplitude2.getValue()+"cos("+spinnerOrdem2.getValue()+"wt"+'+'+spinnerAngulo2.getValue()+"°)"+'+'+Amplitude3.getValue()+"cos("+spinnerOrdem3.getValue()+"wt"+'+'+spinnerAngulo3.getValue()+"°)"+'+'+Amplitude4.getValue()+"cos("+spinnerOrdem4.getValue()+"wt"+'+'+spinnerAngulo4.getValue()+"°)"));
				}
			else if((Integer)spinnerHarmonicas.getValue() == 5) {
				lblRes.setText(String.valueOf("f(t)="+Amplitude.getValue()+"cos(wt)"+'+'+Amplitude1.getValue()+"cos("+spinnerOrdem1.getValue()+"wt)"+'+'+Amplitude2.getValue()+"cos("+spinnerOrdem2.getValue()+"wt"+'+'+spinnerAngulo2.getValue()+"°)"+'+'+Amplitude3.getValue()+"cos("+spinnerOrdem3.getValue()+"wt"+'+'+spinnerAngulo3.getValue()+"°)"+'+'+Amplitude4.getValue()+"cos("+spinnerOrdem4.getValue()+"wt"+'+'+spinnerAngulo4.getValue()+"°)"+'+'+Amplitude5.getValue()+"cos("+spinnerOrdem5.getValue()+"wt"+'+'+spinnerAngulo5.getValue()+"°)"));
			}
			if((Integer)spinnerHarmonicas.getValue() == 0) {

				lblResultado.setBounds(50, 300, 198, 15);
				panel7.setBounds(10,300,300,175);
				lblSerieFourier.setBounds(450, 300, 267, 15);
				lblSaida.setBounds(300, 260, 176, 24);
				lblRes.setBounds(450, 330, 267, 15);
				panel2.setVisible(false);
				panel3.setVisible(false);
				panel4.setVisible(false);
				panel5.setVisible(false);
				panel6.setVisible(false);
				lblAmplitude1.setVisible(false);
				Amplitude1.setVisible(false);
				lblAmplitude2.setVisible(false);
				Amplitude2.setVisible(false);
				lblAmplitude3.setVisible(false);
				Amplitude3.setVisible(false);
				lblAmplitude4.setVisible(false);
				Amplitude4.setVisible(false);
				lblAmplitude5.setVisible(false);
				Amplitude5.setVisible(false);
				lblAngulo1.setVisible(false);
				spinnerAngulo1.setVisible(false);
				lblOrdem1.setVisible(false);
				spinnerOrdem1.setVisible(false);
				lblOrdem2.setVisible(false);
				spinnerOrdem2.setVisible(false);
				lblAngulo3.setVisible(false);
				spinnerAngulo3.setVisible(false);
				lblOrdem3.setVisible(false);
				spinnerOrdem3.setVisible(false);
				lblAngulo4.setVisible(false);
				spinnerAngulo4.setVisible(false);
				lblOrdem4.setVisible(false);
				spinnerOrdem4.setVisible(false);
				lblAngulo5.setVisible(false);
				spinnerAngulo5.setVisible(false);
				lblOrdem5.setVisible(false);
				spinnerOrdem5.setVisible(false);
				lblAngulo2.setVisible(false);
				spinnerAngulo2.setVisible(false);
				lblAmplu1.setVisible(false);
				spinnerAmplitude1.setVisible(false);
				lblAmplu2.setVisible(false);
				spinnerAmplitude2.setVisible(false);
				lblAmplu3.setVisible(false);
				spinnerAmplitude3.setVisible(false);
				lblAmplu4.setVisible(false);
				spinnerAmplitude4.setVisible(false);
				lblAmplu5.setVisible(false);
				spinnerAmplitude5.setVisible(false);
			}
			
			if((Integer)spinnerHarmonicas.getValue() == 1) {
				lblResultado.setBounds(50, 430, 198, 15);
				panel7.setBounds(10,430,300,175);
				lblSerieFourier.setBounds(450, 430, 267, 15);
				lblSaida.setBounds(300, 390, 176, 24);
				lblRes.setBounds(450, 460, 267, 15);
				panel2.setVisible(true);
				panel3.setVisible(false);
				panel4.setVisible(false);
				panel5.setVisible(false);
				panel6.setVisible(false);
				lblOrdem1.setVisible(true);
				spinnerOrdem1.setVisible(true);
				lblAngulo2.setVisible(false);
				spinnerAngulo2.setVisible(false);
				lblOrdem2.setVisible(false);
				spinnerOrdem2.setVisible(false);
				lblAngulo3.setVisible(false);
				spinnerAngulo3.setVisible(false);
				lblOrdem3.setVisible(false);
				spinnerOrdem3.setVisible(false);
				lblAngulo4.setVisible(false);
				spinnerAngulo4.setVisible(false);
				lblOrdem4.setVisible(false);
				spinnerOrdem4.setVisible(false);
				lblAngulo5.setVisible(false);
				spinnerAngulo5.setVisible(false);
				lblOrdem5.setVisible(false);
				spinnerOrdem5.setVisible(false);
				lblAmplitude1.setVisible(true);
				Amplitude1.setVisible(true);
				lblAmplitude2.setVisible(false);
				Amplitude2.setVisible(false);
				lblAmplitude3.setVisible(false);
				Amplitude3.setVisible(false);
				lblAmplitude4.setVisible(false);
				Amplitude4.setVisible(false);
				lblAmplitude5.setVisible(false);
				Amplitude5.setVisible(false);
				lblAngulo1.setVisible(true);
				spinnerAngulo1.setVisible(true);
				lblAmplu1.setVisible(true);
				spinnerAmplitude1.setVisible(true);
				lblAmplu2.setVisible(false);
				spinnerAmplitude2.setVisible(false);
				lblAmplu3.setVisible(false);
				spinnerAmplitude3.setVisible(false);
				lblAmplu4.setVisible(false);
				spinnerAmplitude4.setVisible(false);
				lblAmplu5.setVisible(false);
				spinnerAmplitude5.setVisible(false);
			}
			
			if((Integer)spinnerHarmonicas.getValue() == 2) {
				lblResultado.setBounds(50, 560, 198, 15);
				panel7.setBounds(10,560,300,175);
				lblSerieFourier.setBounds(450, 560, 267, 15);
				lblSaida.setBounds(300, 520, 176, 24);
				lblRes.setBounds(450, 590, 267, 15);
				panel2.setVisible(true);
				panel3.setVisible(true);
				panel4.setVisible(false);
				panel5.setVisible(false);
				panel6.setVisible(false);
				lblOrdem1.setVisible(true);
				spinnerOrdem1.setVisible(true);
				lblAngulo2.setVisible(true);
				spinnerAngulo2.setVisible(true);
				lblOrdem2.setVisible(true);
				spinnerOrdem2.setVisible(true);
				lblAngulo3.setVisible(false);
				spinnerAngulo3.setVisible(false);
				lblOrdem3.setVisible(false);
				spinnerOrdem3.setVisible(false);
				lblAngulo4.setVisible(false);
				spinnerAngulo4.setVisible(false);
				lblOrdem4.setVisible(false);
				spinnerOrdem4.setVisible(false);
				lblAngulo5.setVisible(false);
				spinnerAngulo5.setVisible(false);
				lblOrdem5.setVisible(false);
				spinnerOrdem5.setVisible(false);
				lblAmplitude1.setVisible(true);
				Amplitude1.setVisible(true);
				lblAmplitude2.setVisible(true);
				Amplitude2.setVisible(true);
				lblAmplitude3.setVisible(false);
				Amplitude3.setVisible(false);
				lblAmplitude4.setVisible(false);
				Amplitude4.setVisible(false);
				lblAmplitude5.setVisible(false);
				Amplitude5.setVisible(false);
				lblAngulo1.setVisible(true);
				spinnerAngulo1.setVisible(true);
				lblAmplu1.setVisible(true);
				spinnerAmplitude1.setVisible(true);
				lblAmplu2.setVisible(true);
				spinnerAmplitude2.setVisible(true);
				lblAmplu3.setVisible(false);
				spinnerAmplitude3.setVisible(false);
				lblAmplu4.setVisible(false);
				spinnerAmplitude4.setVisible(false);
				lblAmplu5.setVisible(false);
				spinnerAmplitude5.setVisible(false);
			}
			
			if((Integer)spinnerHarmonicas.getValue() == 3) {
				lblResultado.setBounds(50, 690, 198, 15);
				panel7.setBounds(10,690,300,175);
				lblSerieFourier.setBounds(450, 690, 267, 15);
				lblSaida.setBounds(300, 660, 176, 24);
				lblRes.setBounds(450, 720, 267, 15);
				panel2.setVisible(true);
				panel3.setVisible(true);
				panel4.setVisible(true);
				panel5.setVisible(false);
				panel6.setVisible(false);
				lblOrdem1.setVisible(true);
				spinnerOrdem1.setVisible(true);
				lblAngulo2.setVisible(true);
				spinnerAngulo2.setVisible(true);
				lblOrdem2.setVisible(true);
				spinnerOrdem2.setVisible(true);
				lblAngulo3.setVisible(true);
				spinnerAngulo3.setVisible(true);
				lblOrdem3.setVisible(true);
				spinnerOrdem3.setVisible(true);
				lblAngulo4.setVisible(false);
				spinnerAngulo4.setVisible(false);
				lblOrdem4.setVisible(false);
				spinnerOrdem4.setVisible(false);
				lblAngulo5.setVisible(false);
				spinnerAngulo5.setVisible(false);
				lblOrdem5.setVisible(false);
				spinnerOrdem5.setVisible(false);
				lblAmplitude1.setVisible(true);
				Amplitude1.setVisible(true);
				lblAmplitude2.setVisible(true);
				Amplitude2.setVisible(true);
				lblAmplitude3.setVisible(true);
				Amplitude3.setVisible(true);
				lblAmplitude4.setVisible(false);
				Amplitude4.setVisible(false);
				lblAmplitude5.setVisible(false);
				Amplitude5.setVisible(false);
				lblAngulo1.setVisible(true);
				spinnerAngulo1.setVisible(true);
				lblAmplu1.setVisible(true);
				spinnerAmplitude1.setVisible(true);
				lblAmplu2.setVisible(true);
				spinnerAmplitude2.setVisible(true);
				lblAmplu3.setVisible(true);
				spinnerAmplitude3.setVisible(true);
				lblAmplu4.setVisible(false);
				spinnerAmplitude4.setVisible(false);
				lblAmplu5.setVisible(false);
				spinnerAmplitude5.setVisible(false);
			}
			
			if((Integer)spinnerHarmonicas.getValue() == 4) {
				lblResultado.setBounds(50, 820, 198, 15);
				panel7.setBounds(10,820,300,175);
				lblSerieFourier.setBounds(450, 820, 267, 15);
				lblSaida.setBounds(300, 790, 176, 24);
				lblRes.setBounds(450, 850, 267, 15);
				panel2.setVisible(true);
				panel3.setVisible(true);
				panel4.setVisible(true);
				panel5.setVisible(true);
				panel6.setVisible(false);
				lblOrdem1.setVisible(true);
				spinnerOrdem1.setVisible(true);
				lblAngulo2.setVisible(true);
				spinnerAngulo2.setVisible(true);
				lblOrdem2.setVisible(true);
				spinnerOrdem2.setVisible(true);
				lblAngulo3.setVisible(true);
				spinnerAngulo3.setVisible(true);
				lblOrdem3.setVisible(true);
				spinnerOrdem3.setVisible(true);
				lblAngulo4.setVisible(true);
				spinnerAngulo4.setVisible(true);
				lblOrdem4.setVisible(true);
				spinnerOrdem4.setVisible(true);
				lblAngulo5.setVisible(false);
				spinnerAngulo5.setVisible(false);
				lblOrdem5.setVisible(false);
				spinnerOrdem5.setVisible(false);
				lblAmplitude1.setVisible(true);
				Amplitude1.setVisible(true);
				lblAmplitude2.setVisible(true);
				Amplitude2.setVisible(true);
				lblAmplitude3.setVisible(true);
				Amplitude3.setVisible(true);
				lblAmplitude4.setVisible(true);
				Amplitude4.setVisible(true);
				lblAmplitude5.setVisible(false);
				Amplitude5.setVisible(false);
				lblAngulo1.setVisible(true);
				spinnerAngulo1.setVisible(true);
				lblAmplu1.setVisible(true);
				spinnerAmplitude1.setVisible(true);
				lblAmplu2.setVisible(true);
				spinnerAmplitude2.setVisible(true);
				lblAmplu3.setVisible(true);
				spinnerAmplitude3.setVisible(true);
				lblAmplu4.setVisible(true);
				spinnerAmplitude4.setVisible(true);
				lblAmplu5.setVisible(false);
				spinnerAmplitude5.setVisible(false);
			}
			
			if((Integer)spinnerHarmonicas.getValue() == 5) {
				lblResultado.setBounds(50, 950, 198, 15);
				panel7.setBounds(10,950,300,175);
				lblSerieFourier.setBounds(450, 950, 267, 15);
				lblSaida.setBounds(300, 920, 176, 24);
				lblRes.setBounds(450, 980, 267, 15);
				panel2.setVisible(true);
				panel3.setVisible(true);
				panel4.setVisible(true);
				panel5.setVisible(true);
				panel6.setVisible(true);
				lblOrdem1.setVisible(true);
				spinnerOrdem1.setVisible(true);
				lblAngulo2.setVisible(true);
				spinnerAngulo2.setVisible(true);
				lblOrdem2.setVisible(true);
				spinnerOrdem2.setVisible(true);
				lblAngulo3.setVisible(true);
				spinnerAngulo3.setVisible(true);
				lblOrdem3.setVisible(true);
				spinnerOrdem3.setVisible(true);
				lblAngulo4.setVisible(true);
				spinnerAngulo4.setVisible(true);
				lblOrdem4.setVisible(true);
				spinnerOrdem4.setVisible(true);
				lblAngulo5.setVisible(true);
				spinnerAngulo5.setVisible(true);
				lblOrdem5.setVisible(true);
				spinnerOrdem5.setVisible(true);
				lblAmplitude1.setVisible(true);
				Amplitude1.setVisible(true);
				lblAmplitude2.setVisible(true);
				Amplitude2.setVisible(true);
				lblAmplitude3.setVisible(true);
				Amplitude3.setVisible(true);
				lblAmplitude4.setVisible(true);
				Amplitude4.setVisible(true);
				lblAmplitude5.setVisible(true);
				Amplitude5.setVisible(true);
				lblAngulo1.setVisible(true);
				spinnerAngulo1.setVisible(true);
				lblAmplu1.setVisible(true);
				spinnerAmplitude1.setVisible(true);
				lblAmplu2.setVisible(true);
				spinnerAmplitude2.setVisible(true);
				lblAmplu3.setVisible(true);
				spinnerAmplitude3.setVisible(true);
				lblAmplu4.setVisible(true);
				spinnerAmplitude4.setVisible(true);
				lblAmplu5.setVisible(true);
				spinnerAmplitude5.setVisible(true);
			}
		}
		catch(NumberFormatException exp){
			System.exit(0);
			}
		}
}
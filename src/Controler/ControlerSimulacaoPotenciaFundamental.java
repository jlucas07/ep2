package Controler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.Math;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JSpinner;
import javax.swing.JTextField;

import Model.GraphPanel;

public class ControlerSimulacaoPotenciaFundamental implements ActionListener {
	
	private JProgressBar Veff;
	private JProgressBar Aeff; 
	
	private JSpinner spinnerAmplitudeTensao;
	private JSpinner spinnerAmplitudeCorrente;
	private JSpinner spinnerAnguloTensao;
	private JSpinner spinnerAnguloCorrente;
	
	private GraphPanel panelPotencia;
	private GraphPanel panelCorrente;
	private GraphPanel panelTensao;
	
	private JTextField textPotenciaAtiva;
	private JTextField textPotenciaReativa;
	private JTextField textPotenciaAparente;
	private JTextField textFatorPotencia; 
	
	private int MaxDatePoints;

	public ControlerSimulacaoPotenciaFundamental(	   JProgressBar Veff, JProgressBar Aeff, 
													   JSpinner spinnerAmplitudeTensao, JSpinner spinnerAmplitudeCorrente, JSpinner spinnerAnguloTensao, JSpinner spinnerAnguloCorrente, 
													   GraphPanel panelPotencia, GraphPanel panelCorrente, GraphPanel panelTensao,
													   int MaxDatePoints,
													   JTextField textPotenciaAtiva, JTextField textPotenciaReativa, JTextField textPotenciaAparente, JTextField textFatorPotencia 
													   ){
		
		this.Veff = Veff;
		this.Aeff = Aeff;
		this.spinnerAmplitudeCorrente = spinnerAmplitudeCorrente;
		this.spinnerAmplitudeTensao = spinnerAmplitudeTensao;
		this.spinnerAnguloTensao = spinnerAnguloTensao;
		this.spinnerAnguloCorrente = spinnerAnguloCorrente;
		this.panelPotencia = panelPotencia;
		this.panelCorrente = panelCorrente;
		this.panelTensao = panelTensao;
		this.textFatorPotencia = textFatorPotencia;
		this.textPotenciaAparente = textPotenciaAparente;
		this.textPotenciaAtiva = textPotenciaAtiva;
		this.textPotenciaReativa = textPotenciaReativa;
		this.MaxDatePoints = MaxDatePoints;
	}

	public ControlerSimulacaoPotenciaFundamental(float tensao, float corrente, float angulo) {
		// TODO Auto-generated constructor stub
	}

	public void actionPerformed(ActionEvent arg){
		
		Veff.setValue((Integer)spinnerAmplitudeTensao.getValue());
		Aeff.setValue((Integer)spinnerAmplitudeCorrente.getValue());
		
		List<Double> scores = new ArrayList<>();
		for (int i = 0; i < MaxDatePoints; i++) {
	          scores.add((double)Veff.getValue()*Math.cos(Math.toRadians(2 * Math.PI*60*i+(Integer)spinnerAnguloTensao.getValue())));
	        }
		
		panelPotencia.setScores(scores);
		
		List<Double> scores2 = new ArrayList<>();
		for (int i = 0; i < MaxDatePoints; i++) {
	          scores2.add((double)Aeff.getValue()*Math.cos(Math.toRadians(2 * Math.PI*60*i+(Integer)spinnerAnguloCorrente.getValue())));
	        }
		panelCorrente.setScores(scores2);
		
		List<Double> scores3 = new ArrayList<>();
        for (int i = 0; i < MaxDatePoints; i++) {
          scores3.add((double)Veff.getValue()*Math.cos(Math.toRadians(2 * Math.PI*60*i+(Integer)spinnerAnguloTensao.getValue()))*(double)(Aeff.getValue()*Math.cos(Math.toRadians(2 * Math.PI*60*i+(Integer)spinnerAnguloCorrente.getValue()))));
        }
        panelTensao.setScores(scores3);
		try {
			spinnerAnguloCorrente.commitEdit();
		} catch ( java.text.ParseException e ) { }
		int value = (Integer) spinnerAnguloTensao.getValue();
		try {
			spinnerAnguloTensao.commitEdit();
		} catch ( java.text.ParseException e ) { }
		int value2 = (Integer) spinnerAnguloCorrente.getValue();
		if(value > 180 || value < -180 || value2 > 180 || value2 < -180) {
			JOptionPane.showMessageDialog(null,"ERROR! ");
		}
		else {
			
		try{
		
		textPotenciaAtiva.setText(String.valueOf((float)(Aeff.getValue()*Veff.getValue()*((float)Math.cos(Math.toRadians(value-value2))))+"  W"));
		textPotenciaReativa.setText(String.valueOf((float)(Aeff.getValue()*Veff.getValue()*((float)Math.sin(Math.toRadians(value-value2))))+"  VAR"));
		textPotenciaAparente.setText(String.valueOf((float) Aeff.getValue()*Veff.getValue()+ "  VA"));
		textFatorPotencia.setText(String.valueOf((float)Math.cos((Math.toRadians(value-value2)))));
		}
		catch(NumberFormatException exp){
			System.exit(0);
			}
		}
	}



}

package Controler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import View.DistorcaoHarmonica;
import View.SimulacaoPotenciaFundamental;

public class ControlerMenuPrincipal implements ActionListener {
	
	public ControlerMenuPrincipal(){}
	
	public void actionPerformed(ActionEvent e){
		String opcao = e.getActionCommand();
		
		if (opcao.equals("Fluxo de Potência Fundamental")){
			SimulacaoPotenciaFundamental.main(null);
		}
		
		else if (opcao.equals("Distorção Harmônica")){
			DistorcaoHarmonica.main(null);;
	}
 
}
}
